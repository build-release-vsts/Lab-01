# CI/CD with Microsoft AzureDevops
Lab 01: Deploy an Agent on Windows

---

# Preparations
 - There s one build server provided for the agent 
 
 - Get build server IP (ask the instructor)
 
 - Credentials: 
 username:**sela** , password:**sela**

---

# Tasks

 - Create an Azure DevOps organization
 
 - Create a Team Project for the workshop
 
 - Download the Agent and create a new Agent Pool
 
 - Create a Personal Access Token (PAT)
 
 - Configure an Agent
 
 - Install npm and add a capability

--- 
 
## Create an Azure DevOps organization

&nbsp;

 - Inside the build server rowse to the Azure DevOps main page and login:

```
https://dev.azure.com
```

&nbsp;

 - Create a new organization by clicking "New Organization":
 

<img alt="Image 1.1" src="images/task-1.1.png"  width="75%" height="75%">

&nbsp;

 - Click "Continue" to accepts the terms and conditions:
 

<img alt="Image 1.2" src="images/task-1.2.png"  width="75%" height="75%">

&nbsp;

 - **Set a name and a region for your Azure DevOps organization:**
 

<img alt="Image 1.3" src="images/task-1.3.png"  width="75%" height="75%">

&nbsp;

 - Wait until the organization is ready  


&nbsp;

---

## Create a Team Project for the workshop

&nbsp;

 - Browse to the organization main page:

```
https://dev.azure.com/<your-organization>
```
 
 &nbsp;
 
 - Create a new project called "**ci-cd-workshop**":
 

<img alt="Image 2.1" src="images/task-2.1.png"  width="75%" height="75%">

&nbsp;

 - Ensure the project was successfully created:
 

<img alt="Image 2.2" src="images/task-2.2.png"  width="75%" height="75%">

&nbsp;
 
---

## Download the Agent and create a new Agent Pool

 - **Please connect to your Server**
 
 - Please open a browser with the azure devops console

 - Browse to "Project Settings -> Agent Pools" page:
 ```
https://dev.azure.com/<organization>/<project>/_settings/agentqueues
```
- Or via console
 <img alt="Image 1.3" src="images/project_settings.jpg"  width="75%" height="75%">


 - Create a new "Agent Pool" by click "Add Pool":
  

<img alt="Image create-pool-01" src="images/create-pool-01.PNG"  width="75%" height="75%">

 - Select the "**Self Hosted**" choice
 - Call it "my-pool" and grant access to all pipelines:
  

<img alt="Image create-pool-02" src="images/create-pool-02.PNG"  width="75%" height="75%">


 - Access to the created Agent Pool:
  

<img alt="Image access-pool" src="images/access-pool.PNG"  width="75%" height="75%">


 - Click "New Agent" to get the agent download link:
  

<img alt="Image download-agent-01" src="images/download-agent-01.PNG"  width="75%" height="75%">


 - Select "Windows" and click "Download" to download the agent:
  

<img alt="Image download-agent-02" src="images/download-agent-02.PNG"  width="75%" height="75%">


 - Extract the zip file in the path "C:\Agent":


<img alt="Image extract-agent" src="images/extract-agent.PNG"  width="75%" height="75%">


---

## Create a Personal Access Token (PAT)

 - Open your profile and go to your security details:
 - Follow the images and thier highlight:

<img alt="Image download-agent-02" src="images/pat1.jpg"  width="75%" height="75%">

 - Select "Personal Access Token" and click "**New Token**" to create the token:

<img alt="Image download-agent-02" src="images/pat2.jpg"  width="75%" height="75%">

 - Create your token with **Full access permisions**: 

<img alt="Image download-agent-02" src="images/pat3.jpg"  width="75%" height="75%">

 - When you're done, make sure to copy the token
 
---

## Configure an Agent

 - Open s Powershell as **Administrator**! and move to the agent folder:

```
$ cd C:\Agent
```

 - Start the configuration by run:

```
$ .\config.cmd
```

 - Enter server URL:

```
$ https://dev.azure.com/<organization>
```

 - Enter authentication type (personal access token):

```
$ PAT
```

 - Enter personal access token:

```
$ <Personal-Access-Token>
```

 - Enter agent pool:

```
$ my-pool
```

 - Enter agent name:

```
$ my-agent
```

 - In the "Perform unzip.." enter :

```
$ N
```

 - Enter work folder:

```
$ _work
```

 - Enter run agent as service?:

```
$ y
```

 - Enter User account to use for the service:

```
$ sela
```

 - Enter User password to use for the service:

```
$ sela
```

 <img alt="Image configure-agent" src="images/configure-agent.PNG"  width="75%" height="75%">


 - Browse to the "**Agent Pools**" -> **My-Pool** page and ensure the Agent was created and is enabled

 <img alt="Image test-agent" src="images/test-agent.PNG"  width="75%" height="75%">
 
---

## Install npm and add a capability

 - Click the agent to see the agent details and inspect the agent capabilities to confirm that npm is not available:
 

<img alt="Image agent-capabilities-01" src="images/agent-capabilities-01.PNG"  width="75%" height="75%">


 - Install npm installer downloaded on the **desktop** or download from:

```
https://nodejs.org/en/
```

 - Open a new CMD terminal and ensure that npm was installed successfully:

```
$ npm -v
```

 - Let's inspect the agent capabilities again to see if nodejs and npm were added to the list (they are not added)
 
 - Please refresh the page

 - Open "**services**" in the build machine and restart the "**Azure Pipelines Agent**" service:
 

<img alt="Image restart-agent" src="images/restart-agent.PNG"  width="75%" height="75%">


 - Let's inspect the agent capabilities again (refresh the page) and you will see npm in the capabilities list
  

 <img alt="Image agent-capabilities-02" src="images/agent-capabilities-02.PNG"  width="75%" height="75%">


